<?

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

// Тест ========================================================

class TestClass {
    
    function test_debug () {
        Debugger::add( "TestClass work", __CLASS__ );
    }
}

class TestClass2 {
    
    function test_debug () {
        Debugger::add( "TestClass2 work", __CLASS__ );
    }
}

TestClass::test_debug();
TestClass2::test_debug();

debug( "simple");
Debugger::add( "simple yes", "yes", "name");
Debugger::add( "simple no", "no", "name");

$displaying_categories = array( "TestClass", "yes" );

Debugger::$displaying_categories = $displaying_categories;



Debugger::output(  );

//Debugger::output( $displaying_categories );
?>