<?

define("WORK_MODE", "CLI");

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

// Тест ========================================================

require_once 'models/SyncModelData.php';
require_once "classes/Model/ModelField.php";

class TestModel extends SyncModelData {
    static
        $table_name = 'data'
        ;
        
    static    
        $name
        ;
        
    static public function set_custom_settings( ){
        TestModel::$name->input_format = '2"#var"2';
        TestModel::$name->output_format = '1"#var"1';
    }
}


TestModel::$name = new CharField();

TestModel::save_default_settings();

$test_data = array(
    array( "name" => "qeweq" )
);

$test = TestModel::ArrayToModelData( $test_data, 'TestModel::set_custom_settings' );
//$test = TestModel::ArrayToModelData( $test_data , 'TestModel::set_custom_settings' );

Debugger::add( $test, null , "test" );
Debugger::output( );

?>