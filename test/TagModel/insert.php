<?

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

require_once 'models/TagModel.php';

// Тест
$test_data = array();
$test_data[] = array(
    "name" => "новая",
    "order" => 1,
    "update_date" => "02.01.2000 1:30:00",
);

$test_data[] = array(
    "name" => "новая2",
    "order" => 2,
    "update_date" => "02.01.2000 1:30:00",
);


//Set DB data
$tagData = TagModel::ArrayToModelData( $test_data , 'TagModel::set_custom_settings' );

$tagData->insert();

Debugger::output( array() );
?>