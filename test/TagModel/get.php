<?

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

require_once 'models/TagModel.php';

// Тест

// Get DB data
$tagData = TagModel::getAll();
Debugger::add( $tagData );

Debugger::output( array() );
?>