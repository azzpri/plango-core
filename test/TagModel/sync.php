<?

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

require_once 'models/TagModel.php';

// Client data
$test_data = array();

$test_data[] = array(
    "name" => "новая",
    "update_date" => "02.01.2000 1:30:00",
);

$test_data[] = array(
    "id" => 1,
    "name" => "обновление",
    "update_date" => "02.01.2000 1:30:00",
);

$test_data[] = array(
    "id" => 2,
    "name" => "удалить",
    "update_date" => "02.01.2000 1:30:00",
    "to_delete" => 1,
    "delete_mode" => "soft"
);


TagModel::syncData( $test_data, TagModel, 'TagModel::set_custom_settings' );

$updatedData = TagModel::getAll();
    
// Parse ModelData to array format
$updatedDataArray = TagModel::ModelDataToArray( $updatedData, 'TagModel::set_custom_settings' );

Debugger::add( $updatedDataArray );

Debugger::output( array() );
?>