<?

require_once 'settings.php';
require_once 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require_once 'classes/Debugger.php';
Debugger::add("Hello! We start right now (debugger initial)");

require_once 'models/TaskModel.php';

// Тест
$test_data = array();
$test_data[] = array(
    "name" => "новая",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
);

$test_data[] = array(
    "name" => "новая2",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
);


//Set DB data
$taskData = TaskModel::ArrayToModelData( $test_data, 'TaskModel::set_custom_settings' );

Debugger::add( $taskData );

$taskData->insert();


Debugger::output( array('DB') );
?>