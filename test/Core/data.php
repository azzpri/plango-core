<?
// ====== TAG ===========================
$tag_data = array();

$tag_data[] = array(
    "name" => "новая",
    "update_date" => "02.01.2000 1:30:00",
);

$tag_data[] = array(
    "id" => 1,
    "name" => "обновление",
    "update_date" => "02.01.2000 1:30:00",
);

$tag_data[] = array(
    "id" => 2,
    "name" => "удалить",
    "update_date" => "02.01.2000 1:30:00",
    "to_delete" => 1,
    "delete_mode" => "soft"
);

// ====== TASK ===========================
$task_data = array();

$task_data[] = array(
    "name" => "новая",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,

);

$task_data[] = array(
    "id" => 1,
    "name" => "обновление",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
);

$task_data[] = array(
    "id" => 2,
    "name" => "удалить",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
    "to_delete" => 1
);


?>