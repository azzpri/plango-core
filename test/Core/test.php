<?
/* Инициализация приложения */
require 'settings.php';
require 'sys.php';


defined(DEBUG) || define("DEBUG", false);

// Подключение дебаггера
require 'classes/Debugger.php';
Debugger::add( "Hello! We start right now (debugger initial)" );

require 'models/TaskModel.php';
require 'models/TagModel.php';

require 'classes/UniversalData.php';
require 'classes/Сore.php';

// Тест

require 'data.php';

$client_test_data = array(
    "tag" => $tag_data,
    "task" => $task_data,
);

$ans = Core::sync( $client_test_data );

// Вывод лога
//Debugger::output( array() );
Debugger::output(  );
?>