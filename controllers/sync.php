<?
if (strcasecmp('XMLHttpRequest', $_SERVER['HTTP_X_REQUESTED_WITH']) !== 0)
    die();

require '../initial.php';

require 'models/TaskModel.php';
require 'models/TagModel.php';
require 'classes/Сore.php';
    
$return = array(
    "debug" => "",
    "is_error" => 0,
    "data" => array(),
);

function quit ( $return ) {
    $return["debug"] = Debugger::string_output();
    $return = json_encode( $return );
    print_r( $return );
    die();
}

Debugger::add( "start ajax server sync" );    


if ( ! isset( $_POST['name'] ) ){
    Debugger::add( "Error: no name in POST" );
    quit();
}

$data = isset( $_POST['data'] ) ?
        $_POST['data'] :
        array();
    
if ( $_POST['name'] == 'task' ) {
    TaskModel::syncData( $data, TaskModel, 'TaskModel::set_custom_settings' );
    $updatedData = TaskModel::getAll();    
    $updatedDataArray = TaskModel::ModelDataToArray( $updatedData, 'TaskModel::set_custom_settings' );
}
if ( $_POST['name'] == 'tag' ) {
    TagModel::syncData( $data, TagModel, 'TagModel::set_custom_settings' );
    $updatedData = TagModel::getAll();    
    $updatedDataArray = TagModel::ModelDataToArray( $updatedData, 'TagModel::set_custom_settings' );
}
$return["data"] = $updatedDataArray;

quit( $return );
?>