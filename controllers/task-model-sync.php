<?
//if (strcasecmp('XMLHttpRequest', $_SERVER['HTTP_X_REQUESTED_WITH']) !== 0)
//    die();

require '../initial.php';

require_once 'models/TaskModel.php';

// Client data
$test_data = array();

$test_data[] = array(
    "name" => "новая",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,

);

$test_data[] = array(
    "id" => 1,
    "name" => "обновление",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
);

$test_data[] = array(
    "id" => 2,
    "name" => "удалить",
    "start_date" => "01.01.2000",
    "start_time" => "01:00",
    "end_date" => "02.01.2000",
    "end_time" => "01:00",
    "remind_date" => "02.01.2000 00:30",
    "update_date" => "02.01.2000 1:30:00",
    "complite" => 0,
    "to_delete" => 1
);


TaskModel::syncData( $test_data, TaskModel, 'TaskModel::set_custom_settings' );

$updatedData = TaskModel::getAll();
    
Debugger::add( $updatedData, null, "updatedData" );    
// Parse ModelData to array format
$updatedDataArray = TaskModel::ModelDataToArray( $updatedData, 'TaskModel::set_custom_settings' );

Debugger::add( $updatedDataArray, null, "updatedDataArray" );

Debugger::output( array() );
?>