<?
/**
 *  Core base class
 */
class Core {
    /*
     * Sync procedure
     *  @param $client_data_array - client data as array
     */
    static public function sync( $client_data_array ){
        
        $return = array( "tag","task" );
        
        // sync tags
        
        if ( isset( $client_data_array['tag'] )  ){
            
            TagModel::syncData( $client_data_array['tag'], TagModel, 'TagModel::set_custom_settings' );
            
            $updated_tag_data = TagModel::getAll();
            $updated_tag_data_array = TagModel::ModelDataToArray(
                $updated_tag_data, 'TagModel::set_custom_settings' );
            $return['tag'] = $updated_tag_data_array;
            
            debug( $updated_tag_data_array , __CLASS__, "tag_data" );
        }
        
        // sync tasks
        if ( isset( $client_data_array['task'] )  ){
            
            TaskModel::syncData( $client_data_array['task'], TaskModel, 'TaskModel::set_custom_settings' );
            
            $updated_task_data = TaskModel::getAll();
            $updated_task_data_array = TaskModel::ModelDataToArray(
                $updated_task_data, 'TaskModel::set_custom_settings' );
            $return['task'] = $updated_task_data_array;
            
            debug( $updated_task_data_array , __CLASS__, "task_data" );
        }
        
        return $return;
    }
        
    /*
     *  Функция вывод данных в дебаггер
     *  @param $filds - перечень полей, которые нужно вывести (default id)
     */
    /*
    static private function debug_data ( $type, $data, $filds = array("id") ) {
        
        if ( ! $type ) {
            Debugger::add("Can't debug data!", "type is not define");
            return;
        }
        
        $display_info = "";
        foreach ( $data as $data_row ) {
            
            $display_row = "";
            
            foreach ( $filds as $fild ) {
                $display_row .= " {$fild} : {$data_row->$fild},";
            }
            
            $display_info .= "({$display_row}),". EOL;
        }
    
        Debugger::add( "{$type} data:" , $display_info );
    }*/

}