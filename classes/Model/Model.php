<?
require_once 'DBModel.php';

/*
 *  Abstract data model 
 */
class Model extends DBModel {
    
    public function getAll ( $custom_data_source = null ) {
        
        $called_class = get_called_class();
        
        return new ModelData(
            ! isset ( $custom_data_source ) ?
                self::db__getAll() :
                $custom_data_source, $called_class );
    
    }

    /*
     *  Get class ModelField-s attributes as list
     */
    protected static function __getClassModelFields ( $called_class ){
        
        $model_fields_keys = array();
        $class_vars = get_class_vars( $called_class );        
        
        foreach ( $class_vars as $var_index => $var ){
            
            if ( self::check_if_parent_is_MF( $var ) ) {
                $model_fields_keys[] = array( "obj" => $var, "name" => $var_index );
            }
        }
        
        return $model_fields_keys;
    }
    
    /*
     *  Function to check if selected class var
     *  parent class is ModelField in recusrion
     *  
     */
    protected static function check_if_parent_is_MF ( $var ) {
        $var_class = get_parent_class( $var );
        
        if( strlen( $var_class ) == 0 )
            return false;
        
        if( $var_class == "ModelField" ){
            return true;
        }else{
            return self::check_if_parent_is_MF( $var_class );
        }
    }
}

require_once 'DBModelData.php';
require_once 'ModelData.php';
?>