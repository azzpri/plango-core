<?
class ModelField {
    public
        $required = false
    ,   $null_value = 'null'
    ,   $use_in_db = true
    ,   $add_quotes = false
    ;
    
    protected
        $default_attributes = array();
        
    /*
     *  Save/reinitialize defaut settings
     */
    public function save_default_settings () {
        foreach( get_object_vars($this) as $key => $value ){
            $this->default_attributes[$key] = $value;
        }
    }
    public function set_default_settings () {
        foreach( $this->default_attributes as $key => $value ){
            $this->$key = $value;
        }
    }
    
    /*
     *  default functions to render value set/get
     */
    public function render_value_set ( $value, $remove_quotes = null ) {
        if( ! isset( $value ) ){
            $value = $this->null_value;
        }
        
        if( isset ( $this->add_quotes ) )
            $value = "'{$value}'";

        if( isset( $remove_quotes ) ){
            $value = str_replace("'", '', $value );
        }
        return $value;
    }
    
    public function render_value_get ( $value ) {
        return $value;
    }
}

class IntField extends ModelField {
  
}

class CharField extends ModelField {
    public
        $null_value = "''"
    ,   $add_quotes = true
    ;
}

class DateField extends ModelField {
    public 
        $datetime_format = "Y-m-d"
    ,   $null_value = "''"
    ,   $add_quotes = true
    ;
    
    public function render_value_get ( $data ) {
        return DateTime::createFromFormat( $this->datetime_format, $data );
    }
    
    public function render_value_set ( $data, $remove_quotes = null  ) {
        
        $value = isset ( $data ) ?
            isset ( $this->add_quotes ) ?
                "'".date_format( $data, $this->datetime_format )."'" :
                date_format( $data, $this->datetime_format ) :
            $this->null_value;
            
        if( isset( $remove_quotes ) ){
            $value = str_replace("'", '', $value );
        }
        
        return $value;
    }
    
}

class TimeField extends DateField {
    public 
        $datetime_format = "H:i:s"
    ;
}

class DateTimeField extends DateField {
    public 
        $datetime_format = "Y-m-d H:i:s"
    ;   
}

class ForeignKey extends IntField {
    public
        $reference_model = ""
    ;
        
    public function __construct( $reference_model ){
        $this->reference_model = $reference_model;
    }
    
    public function render_value_set ( $value, $remove_quotes = null ) {
        if( ! isset( $value ) ){
            $value = $this->null_value;
        }
        
        return $value;
    }
}

?>