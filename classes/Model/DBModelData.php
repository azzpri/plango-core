<?

/*
 *  ModelData db operations
 */
class DBModelData extends Model {
    /*
     *  Method to prepare query to delete data
     *  @param $data - ModelData object
     */
    protected function __delete ( $data ) {
        
        // 1. Find out table name
        $data_class = $data->data_class;
        $table_name = $data_class::$table_name;
        
        $query_string_to_delete = "";
        $delete_children_mode = null;
        
        foreach ( $data->data as $data_row ){
            
            isset( $data_row['delete_mode'] ) ?
                $delete_children_mode = $data_row['delete_mode'] : true;
            
            if ( ! isset( $data_row['id'] ) )
                continue;
            
            if ( strlen( $query_string_to_delete ) > 0 )
                $query_string_to_delete .= ", ";
                
            $query_string_to_delete .= $data_row['id'];    
        }
        
        if ( strlen( $query_string_to_delete ) == 0 )
            return;
        
        $query =
                "delete from {$table_name} where id in ({$query_string_to_delete});";
                
        // delete table references
        if( isset( $delete_children_mode ) ) {
            
            foreach ( $data_class::$references as $reference_model ){
            
                foreach( self::__getClassModelFields( $reference_model ) as $mf ){
                    
                    if( get_class( $mf['obj'] ) == 'ForeignKey' ){
                        
                        if( $mf['obj']->reference_model == $data_class ){
                            
                            $child_table_name = $reference_model::$table_name;
                            $child_column_name = $mf['name'];
                            
                            if ( $delete_children_mode == "soft"){
                                $q = "update {$child_table_name} set {$child_column_name} = null
                                      where {$child_column_name} in ({$query_string_to_delete})";
                            }else{
                                $q = "delete from {$child_table_name} where {$child_column_name} in
                                    ($query_string_to_delete);";
                            }
                            self::__getFromDB( $q );       
                        }
                    }
                }
            }
        }
        
        self::__getFromDB( $query );       
    }
    /*
     *  Method to prepare query for insert or update database *********************************
     *  @param $data - ModelData object
     *  @param $action - "insert" or "update"
     */
    protected function __update_and_insert ( $data, $action ) {
        
        // 1. Find out table name
        $data_class = $data->data_class;
        $table_name = $data_class::$table_name;
        
        // 2. Prepare string of column names
        $column_names_string = "";
        $column_names_list = array();
        
        foreach ( self::__getClassModelFields( $data->data_class ) as $field_key => $field_value ){
            
            if ( ( $field_value['name'] == 'id' && $action == "insert" ) || ! $field_value['obj']->use_in_db  )
                continue;            
            
            $column_names_string .= strlen( $column_names_string ) > 0 ?
                ", `{$field_value['name']}`" : "`{$field_value['name']}`";
                
            $column_names_list[] = $field_value['name'];
        }
        
        Debugger::add( $column_names_string, __CLASS__, 'column_names_string' );
        
        // 3. Prepare data as string and create query
        
        $query_string_array = array();
        $data_string = "";
        
        foreach ( $data->data as $data_row ){
            
            $data_string_row = "";
            $id = null;
            
            foreach ( $column_names_list as $column_name ){
                
                foreach ( $data_row as $data_name => $data_value ){
                    
                    $value = null;
                    
                    if ( $data_name == $column_name ){
                        
                        if( $column_name == "id" )
                            $id = $data_value;
                        
                        $data_string_row .=  strlen( $data_string_row ) > 0 ? ", " : "";
                        $value = $data_value;
                        
                        break;
                    }
                }
                
                if ( $action == "update" )
                    if ( $column_name == "id" )
                        continue;
                    else
                        $data_string_row .= "`{$column_name}` = ";
            
                $data_string_row .= $data_class::$$column_name->render_value_set( $value );
            }
            
            if ( $action == "update" ){
                $query_string_array[] = "update {$table_name} set " . $data_string_row . " where id = {$id};";
            }
            
            if ( $action == "insert" ){
                $data_string .= strlen( $data_string ) > 0 ? ", ({$data_string_row})" : "({$data_string_row})";
            }
        }
        
        //strlen( $data_string ) > 0 ? $query_string_array[] = $data_string : true;
        
        if ( $action == "insert" ){
            $query_string_array[] = "insert into {$table_name} ({$column_names_string}) values {$data_string}";
        }
        
        // 5. Run method to insert
        foreach ( $query_string_array as $query ) {
            self::db__insert( $query );       
        }
    }
}



?>