<?
/**
 * Base static class that consider low-level methods to communication with database.
 */
class DB {
    public static $connection = null; // database connection resource.

    /**
     * Get exist or create new database connection.
     * @return - database connection resource.
     */
    public static function getConnection ( ) {
        
        ! isset( self::$connection ) ?
            self::$connection = mysql_connect( DB_ADDRESS.':'.DB_PORT, DB_USERNAME, DB_PASSWORD ) : true ;
        
        self::$connection || die('Не могу подключиться к БД');
        
        mysql_set_charset('utf8');
        
        Debugger::add( mysql_client_encoding(), __CLASS__, 'mysql_client_encoding' );
        
        mysql_select_db( DB_NAME );
    }
    
    /**
     *  Get data from database. Return it as associated array.
     *  @param $query_string - Safe query to database. Default "".
     *  @return - array of rows.
     */
    public static function getData_AssocArray ( $query_string = "") {
        
        self::getConnection();
        
        Debugger::add( $query_string, __CLASS__, "query_string" );
        
        $query = mysql_query( $query_string );
        
        if ( ! $query ){
            Debugger::add( mysql_error() );
            return ;
        }
         
        Debugger::add( $query, __CLASS__, "query" );

        $result = array();
        while ( $row = mysql_fetch_assoc( $query ) ) {
            $result[] = $row;
        }
        
        Debugger::add( $result, __CLASS__, "query_result" );
        
        return $result;
        
    }
    
}