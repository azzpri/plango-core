<?

require_once 'DB.php';

/*
 *  Class of abstract models, that consider methods to get/set information from the database.
 */
class DBModel {
    
    protected function db__getAll () {
        $query = "select * from " . self::_modelTableName();
        
        return self::__getFromDB( $query );
    }

    /*
     *  Функция получения информации из базы
     */
    protected static function __getFromDB( $query_string ) {
        return DB::getData_AssocArray( $query_string );
    }
    
    /*
     *  get called function table name
     */
    protected function _modelTableName () {
        $called_class = get_called_class();
        return $called_class::$table_name;
    }
    
    /*
     *  Function insert into table
     *  @param $query_string - string query
     *  @param $return_id - lag to return id of first insert item
     */
    protected function db__insert ( $query_string, $return_first_id = false ) {
        self::__getFromDB( $query_string );
        
        if ( $return_first_id ) {
            $ans = self::__getFromDB( 'select last_insert_id()' );
            return $ans[0]['last_insert_id()'];
        }
  
        return;
    }
}

?>