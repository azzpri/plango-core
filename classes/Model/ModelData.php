<?

/*
 *  Abstract data model data
 */
class ModelData extends DBModelData {
    
    protected
        $data
    ,   $data_class
    ,   $initial_settings
    ;
        
    public function __construct( $raw_data, $called_class ) {
        
        $returned_data = array();
        $this->data_class = $called_class;
        $model_fields_keys = self::__getClassModelFields( $called_class );
        
        foreach ( $raw_data as $raw_data_row ){
            
            $tmp_row = array();
            
            foreach ( $model_fields_keys as $model_field ){
                
                $add_null_value = true;
                
                foreach ( $raw_data_row as $data_index => $data_value ){
                    
                    if ( $model_field["name"] == $data_index ){
                        $tmp_row[ $data_index ] = $model_field["obj"]->render_value_get( $data_value );
                        $add_null_value = false;
                        break;
                    }
                    
                }
                
                if( $add_null_value ){
                    $tmp_row[ $model_field['name'] ] = $model_field["obj"]->null_value;
                }

            }
            
            if( count( $tmp_row ) > 0 ){
                $returned_data[] = $tmp_row;
            }
        }
        
        $this->data = $returned_data;
        
        return $this;
    }
    
    /*
     *  insert and update methods public links 
     */
    public function update (){
        DBModelData::__update_and_insert ( $this, "update" );
    }
    public function insert (){
        DBModelData::__update_and_insert ( $this, "insert" );
    }
    /*
     *  public link to delete method
     */
    public function delete (){
        DBModelData::__delete ( $this );
    }
    
    /*
     *  Hit ModelField subclasses to store they default_settings
     */
    public static function save_default_settings( ){
        
        foreach( get_class_vars( get_called_class() ) as $class_var ){
            
            if ( ! isset( $class_var ) )
                continue;
            
            if ( ! self::check_if_parent_is_MF( $class_var ) )
                continue;
            
            $class_var->save_default_settings();
        }
    }
    
    /*
     *  set initial settings to ModelData instance
     */
    public static function set_initial_settings( $called_class ){
        
        foreach( get_class_vars( $called_class ) as $class_var ){
            
            if ( ! isset( $class_var ) )
                continue;
            
            if ( ! self::check_if_parent_is_MF( $class_var ) )
                continue;
            
            $class_var->set_default_settings();
        }
    }
}
?>