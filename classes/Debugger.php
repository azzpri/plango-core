<?
/*
 *  Core developer concole debugger lib. Store messages during work
 *  in $_SESSION['debug_storage'],
 *  after that display all to console.
 */

/*
 *  Debug record structure.
 */
class DebugRecord {
    public
        $display_name,
        $display_message,
        $display_category
    ;    
}

/*
 *  Base debugger class.
 */
class Debugger {
    
    static
        $displaying_categories = null; // default displaying category list
    
    /*
    * Add message to debugger
    *   @param $display_message
    *   @param $display_category
    *   @param $display_name
    */
    public function add ( $display_message, $display_category = null, $display_name = null  ){
        
        self::checkDebugEnable();
          
        $record = new DebugRecord();
        
        $record->display_message = $display_message;
        $record->display_category = $display_category;
        $record->display_name = $display_name;
        
        $_SESSION["debug_storage"][] = $record;
    }
    
    /*
     * Echo all messages in storage
     *  @param $displaying_categories - list of categories to display.
     */
    public function output ( $displaying_categories = null ) {
        $output = self::string_output( $displaying_categories );
        print_r( $output );
    }
    
    /*
     *  Prepare log as sting
     *  @param $displaying_categories - list of categories to display.
     */
    public function string_output ( $displaying_categories = null ) {    
        ! isset( $displaying_categories ) ? $displaying_categories = self::$displaying_categories : true;
        
        self::checkDebugEnable();
        
        $br = EOL;
        
        $output = "";
        $output .= $br;
        
        foreach( $_SESSION["debug_storage"] as $record ){
            
            if ( isset( $displaying_categories ) ) {
                
                if ( isset( $record->display_category ) ){
                    
                    if( ! in_array( $record->display_category, $displaying_categories ) )
                        continue;
                }
            }
            
            isset( $record->display_name ) ?
                $output .= "{$record->display_name} :{$br}" : true;
            
            $output .= json_encode( $record->display_message );    
            $output .= $br.$br;
        }

        return $output;
    }
    
    /*
     * Echo debugger messages and exit
     *  @param $displaing_categories
     */
    public static function goOut ( $displaing_categories = null ) {
        self::output( $displaing_categories = null );
        exit();
    }
    
    /*
     *  Test debug enable
     */
    private static function checkDebugEnable () {
        if ( ! DEBUG ){
            exit;
        }
    }
    
}

/*
 *  Debugger::add short decorator
 *   @param $display_message
 *   @param $display_category
 *   @param $display_name
 */
function debug (  $display_message, $display_category = null, $display_name = null   ) {
    Debugger::add( $display_message, $display_category = null, $display_name = null );    
}
?>