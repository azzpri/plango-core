<?

/*  Конфигурация приложения */
set_include_path('/var/www/plango/plango-core');

/* Режим отладки */
define("DEBUG", True);

/* Режим работы  CLI, CGI */
define("WORK_MODE", "CLI");

/* Настройка подключения к базе */
define("DB_ADDRESS","");
define("DB_PORT","");
define("DB_USERNAME","");
define("DB_PASSWORD","");
define("DB_NAME","");

?>