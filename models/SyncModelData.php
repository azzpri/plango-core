<?

require_once "classes/Model/Model.php";

class SyncModelData extends ModelData {
    static
        $references = array()
    ;
    /*
     *  Symlink to create ModelData from client data
     *  @param $client_data - client raw data package
     *  @param $custom_set_fn_name - function name changed settings
     */
    public static function ArrayToModelData ( $raw_client_data, $custom_set_fn_name = null ) {
        
        isset ( $custom_set_fn_name ) ? call_user_func( $custom_set_fn_name, get_called_class() ) : true;
        
        $ans = self::getAll( $raw_client_data );
        
        isset ( $custom_set_fn_name ) ? self::set_initial_settings( get_called_class() ) : true;
        
        return $ans;
    }
    
    /*
     *  Convert ModelData to php array
     *  @param $modelData - ModelData object
     *  @param $custom_set_fn_name - function name changed settings
     */
    public static function ModelDataToArray (
            $modelData, $custom_set_fn_name = null ) {
        
        isset ( $custom_set_fn_name ) ? call_user_func( $custom_set_fn_name, get_called_class() ) : true;
        
        $data_array = array();
        $called_class = get_called_class();
        
        $cmf = self::__getClassModelFields( $called_class );
        
        foreach ( $modelData->data as $model_data_row ) {
            
            $tmp_data_row = array();
               
            foreach ( $model_data_row as $model_data_key => $model_data_value ) {
                /* TODO: replace next loop and call in line 24 "$cmf = ..." for function
                 *  ModelData::get_attribute_class( $called_class, $attribute );
                 */
                foreach ( $cmf as $class_attribute ) {
                    if ( $class_attribute['name'] == $model_data_key ) {
                        
                        $tmp_data_row[ $model_data_key ] =
                            $class_attribute['obj']->render_value_set( $model_data_value, true );
                        break;
                    }
                }
            }
            
            $data_array[] = $tmp_data_row;
        }
        
        isset ( $custom_set_fn_name ) ? self::set_initial_settings( get_called_class() ) : true;
        
        return $data_array;
    }
    
    /*  
     *  Sync client data with server:
     *      - update database
     *      - return new ModelData object
     *  @param $client_data - client raw data package
     *  @param $base_class - base Model class of pair (Model, ModelClient)
     *  @param $custom_set_fn_name - function name changed settings
     */
    public static function syncData ( $client_data, $base_class, $custom_set_fn_name = null ){
        
        $serverData = self::getAll();
        $clientData = self::ArrayToModelData ( $client_data, $custom_set_fn_name );
        
        // indexes of client data to update, insert and delete in db
        $data_to_insert = new ModelData( array(), $base_class );
        $data_to_update = new ModelData( array(), $base_class );
        $data_to_delete = new ModelData( array(), $base_class );        
        
        // fill data_to_update, data_to_delete and data_to_insert
        foreach ( $clientData->data as $cData_index => $cData_row ) {
            
            $insert_this_data_row = true;
            
            foreach ( $serverData->data as $sData_row ) {
                
                if ( $cData_row['id'] == $sData_row['id'] ) {
                    
                    $insert_this_data_row = false;
                    
                    //Debugger::add("_data", $cData_row);
                    
                    if( $cData_row["to_delete"] == 1 ) {
                        $data_to_delete->data[] = $cData_row;
                        break;
                    }
                    
                    if ( $cData_row["update_date"] > $sData_row["update_date"] ) {
                        $data_to_update->data[] = $cData_row;
                        break;
                    }
                }
            }
            
            if ( $insert_this_data_row ) {
                $data_to_insert->data[] = $cData_row;
            }
        }
        
        if ( count( $data_to_insert ) > 0 )
            $data_to_insert->insert();
        
        if ( count( $data_to_update ) > 0 )
            $data_to_update->update();
            
        if ( count( $data_to_delete ) > 0 )
            $data_to_delete->delete();
        
        
    }
    
}