<?
require_once "classes/Model/ModelField.php";
require_once "SyncModelData.php";

/*
 *  TaskModel class for db data
 */
class TaskModel extends SyncModelData {
    static
        $table_name = 'data'
        ;
        
    static    
            $id
        ,   $name
        ,   $id_tag
        ,   $start_date
        ,   $start_time
        ,   $end_date
        ,   $end_time
        ,   $remind_date
        ,   $update_date
        ,   $complete
        ,   $to_delete
        ,   $sort_order
        ;
    static public function set_custom_settings( ){
        TaskModel::$start_date->datetime_format = 'd.m.Y';
        TaskModel::$start_time->datetime_format = 'H:i';
        TaskModel::$end_date->datetime_format = 'd.m.Y';
        TaskModel::$end_time->datetime_format = 'H:i';
        TaskModel::$remind_date->datetime_format = 'd.m.Y H:i';
        TaskModel::$update_date->datetime_format = 'd.m.Y H:i:s';
        TaskModel::$name->add_quotes = false;
    }    
}

TaskModel::$id = new IntField();
TaskModel::$name = new CharField();
TaskModel::$id_tag = new ForeignKey( 'TagModel' );
TaskModel::$start_date = new DateField();
TaskModel::$start_time = new TimeField();
TaskModel::$end_date = new DateField();
TaskModel::$end_time = new TimeField();
TaskModel::$remind_date = new DateTimeField();
TaskModel::$update_date = new DateTimeField();
TaskModel::$complete = new IntField();
TaskModel::$to_delete = new IntField();
TaskModel::$to_delete->use_in_db = false;
TaskModel::$sort_order = new IntField();

TaskModel::save_default_settings();
?>