<?
require_once "classes/Model/ModelField.php";
require_once "SyncModelData.php";
require_once "TaskModel.php";

/*
 *  TagModel class for db data
 */
class TagModel extends SyncModelData {
    static
        $table_name = 'tag'
    ,   $references = array( TaskModel )
    ;
        
    static    
            $id
        ,   $name
        ,   $order
        ,   $update_date
        ,   $to_delete
        ,   $delete_mode
        ;
        
    static public function set_custom_settings( ){
        TagModel::$update_date->datetime_format = 'd.m.Y H:i:s';
    }    
}   
    
TagModel::$id = new IntField();
TagModel::$name = new CharField();
TagModel::$order = new IntField();
TagModel::$update_date = new DateTimeField();

TagModel::$to_delete = new IntField();
TagModel::$to_delete->use_in_db = false;

TagModel::$delete_mode = new CharField();
TagModel::$delete_mode->use_in_db = false;

TagModel::save_default_settings();


?>